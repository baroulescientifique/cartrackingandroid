package com.example.cartracker2;


import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

public class ExampleJobService extends JobService {

    private static final String TAG = "ExampleJobService";
    private boolean jobCancelled = false;

    @Override
    public boolean onStartJob(JobParameters params) {

        Log.d(TAG, "Job started");
        doBackgroundWork(params);

        //Reschedule the Service before calling job finished
        /*if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            scheduleRefresh();
        }
*/

        return true;
    }



    private void doBackgroundWork(final JobParameters params) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                for (int i = 0; i < 10; i++) {
                    Log.d(TAG, "run: " + i);
                    if (jobCancelled) {
                        return;
                    }

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                //Toast.makeText(ExampleJobService.this, "User Location data sent", Toast.LENGTH_LONG).show();

                Log.d(TAG, "Job finished");

                Log.d(TAG, "User Location data sent");
                jobFinished(params , false);

            }
        }).start();
    }


   /* private void scheduleRefresh() {
        JobScheduler mJobScheduler = (JobScheduler) getApplicationContext()
                .getSystemService(JOB_SCHEDULER_SERVICE);
        JobInfo.Builder mJobBuilder =
                new JobInfo.Builder(123,
                        new ComponentName(getPackageName(),
                                ExampleJobService.class.getName()));

        *//* For Android N and Upper Versions *//*
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mJobBuilder
                    .setMinimumLatency(60 * 1000) //YOUR_TIME_INTERVAL
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        }
    }*/

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.d(TAG, "Job cancelled before completion");
        jobCancelled = true;
        return true;
    }
}
