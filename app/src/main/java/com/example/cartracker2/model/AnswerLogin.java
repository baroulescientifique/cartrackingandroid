package com.example.cartracker2.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



public class AnswerLogin {

    @SerializedName("trip")
    @Expose
    private Trip trip;

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

}