package com.example.cartracker2;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cartracker2.api.ApiClient;
import com.example.cartracker2.api.ApiService;
import com.example.cartracker2.model.AnswerLogin;
import com.example.cartracker2.model.Driver;
import com.example.cartracker2.model.Trip;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    Button signin ;
    EditText login;
    EditText password ;
    Driver driver;

    public final static String MESSAGE_DRIVER = "";
    public final static String MESSAGE_START = "";
    public final static String MESSAGE_END = "";
    public final static String MESSAGE_CREATED = "";

    public static ApiService apiService ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        apiService = ApiClient.getApiClient().create(ApiService.class);



        signin = findViewById(R.id.sign_in_button);
        login = findViewById(R.id.login);
        password = findViewById(R.id.password);

        signin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                performLoginAction();
            }
        });

    }

    public void performLoginAction(){


        final String username =  login.getText().toString();
        final String userpass = password.getText().toString();

        driver =  new Driver();
        driver.setLogin(username);
        driver.setPassword(userpass);

        Call<AnswerLogin> call = apiService.login(driver);

        call.enqueue(new Callback<AnswerLogin>() {
            @Override
            public void onResponse(Call<AnswerLogin> call, Response<AnswerLogin> response) {


                AnswerLogin mytrip = response.body();


                String Driver = mytrip.getTrip().getDriver().getName();
                String StartTime = mytrip.getTrip().getStartTime();
                String EndTime = mytrip.getTrip().getEndTime();
                String CreatedAt = mytrip.getTrip().getCreatedAt();
                String RouteDetails = mytrip.getTrip().getRoute().getDetails();

                String Login = mytrip.getTrip().getDriver().getLogin();
                String Password = mytrip.getTrip().getDriver().getPassword();


                Intent intent= new Intent(LoginActivity.this, TaskActivity.class);

                intent.putExtra("Driver", Driver);
                intent.putExtra("Start", StartTime);
                intent.putExtra("End", EndTime);
                intent.putExtra("CreatedAt", CreatedAt);
                intent.putExtra("Route", RouteDetails);

                intent.putExtra("Login", Login);
                intent.putExtra("Password", Password);

                startActivity(intent);


            }

            @Override
            public void onFailure(Call<AnswerLogin> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Big failure ", Toast.LENGTH_SHORT).show();
                Log.d("MyError", t.getMessage());
            }
        });

    }
}

