package com.example.cartracker2.api;

import com.example.cartracker2.model.AnswerLogin;
import com.example.cartracker2.model.Driver;
import com.example.cartracker2.model.Trip;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {
    @GET("/api/v1/driver")
    Call<JsonObject> getDrivers();

    @POST("/api/v1/login")
    Call<AnswerLogin> login(@Body Driver driver);
} 
