package com.example.cartracker2;

import android.Manifest;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PointF;
import android.location.Location;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.cartracker2.api.ApiClient;
import com.example.cartracker2.api.ApiService;
import com.example.cartracker2.model.AnswerLogin;
import com.example.cartracker2.model.Driver;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.yandex.mapkit.Animation;
import com.yandex.mapkit.MapKitFactory;
import com.yandex.mapkit.geometry.Circle;
import com.yandex.mapkit.geometry.Point;
import com.yandex.mapkit.geometry.Polyline;
import com.yandex.mapkit.layers.ObjectEvent;
import com.yandex.mapkit.location.LocationListener;
import com.yandex.mapkit.location.LocationManager;
import com.yandex.mapkit.map.CameraPosition;
import com.yandex.mapkit.map.CircleMapObject;
import com.yandex.mapkit.map.CompositeIcon;
import com.yandex.mapkit.map.IconStyle;
import com.yandex.mapkit.map.Map;
import com.yandex.mapkit.map.MapObjectCollection;
import com.yandex.mapkit.map.PlacemarkMapObject;
import com.yandex.mapkit.map.PolylineMapObject;
import com.yandex.mapkit.map.RotationType;
import com.yandex.mapkit.mapview.MapView;
import com.yandex.mapkit.user_location.UserLocationLayer;
import com.yandex.mapkit.user_location.UserLocationObjectListener;
import com.yandex.mapkit.user_location.UserLocationView;
import com.yandex.runtime.image.ImageProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapActivity extends AppCompatActivity implements UserLocationObjectListener {

    private MapView mapView;
    private Driver driver;
    public static ApiService apiService;
    private final String MAPKIT_API_KEY = "b7223f4f-f6e2-4ea6-9f9f-a4988c0ad04a";
    private final Point TARGET_LOCATION = new Point(59.945933, 30.320045);
    private final Point DRAGGABLE_PLACEMARK_CENTER = new Point(59.948, 30.323);

    private MapObjectCollection mapObjects;
    private Handler animationHandler;

    private UserLocationLayer userLocationLayer;
    public Point origin;

    private LocationManager locationManager;
    private LocationListener myLocationListener;
    private Point myLocation;

    public Button startbtn;

    public final int COARSE_LOCATION = 5;

    private static final String TAG = "MapActivity";

    private FusedLocationProviderClient fusedLocationClient;
    private LocationCallback locationCallback;

    public double start_latitude , start_longitude, end_latitude, end_longitude ;
    public boolean isRoadStarted, isRoadTerminated ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        apiService = ApiClient.getApiClient().create(ApiService.class);
        final Intent intent = getIntent();

        //this is what i am going too add here
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        //Lets initialize the road variable
        isRoadStarted  = false;
        isRoadTerminated = false;

        driver = new Driver();
        driver.setLogin(intent.getStringExtra("login"));
        driver.setPassword(intent.getStringExtra("password"));


        MapKitFactory.setApiKey("b7223f4f-f6e2-4ea6-9f9f-a4988c0ad04a");
        MapKitFactory.initialize(this);

        // Укажите имя activity вместо map.
        setContentView(R.layout.activity_map);
        mapView = (MapView) findViewById(R.id.mapview);


        Call<AnswerLogin> call = apiService.login(driver);

        call.enqueue(new Callback<AnswerLogin>() {
            @Override
            public void onResponse(Call<AnswerLogin> call, Response<AnswerLogin> response) {

                //Toast.makeText(MapActivity.this, "Ok", Toast.LENGTH_SHORT).show();

                String details = response.body().getTrip().getRoute().getDetails();

                JsonParser jsonParser = new JsonParser();
                JsonArray arrayFromString = jsonParser.parse(details).getAsJsonArray();

                for (JsonElement objectDetail : arrayFromString) {

                    JsonObject object = objectDetail.getAsJsonObject();
                    String type = object.get("type").getAsString();
                    String name = object.get("name").getAsString();
                    JsonArray coordinates = object.get("coordinates").getAsJsonArray();

                    mapObjects = mapView.getMap().getMapObjects().addCollection();
                    animationHandler = new Handler();

                    if (type.equals("Circle")) {

                        if (name.equals("Start")) {
                            JsonElement array0 = coordinates.getAsJsonArray().get(0);
                            JsonElement array1 = coordinates.getAsJsonArray().get(1);

                            Log.d("CoordinatesCirSTART", array0 + "------" + array1);

                            Log.d("Sep", "-----------------------------------------");

                            start_latitude =  array0.getAsDouble();
                            start_longitude = array1.getAsDouble();

                            CircleMapObject circle = mapObjects.addCircle(
                                    new Circle(new Point(array0.getAsDouble(), array1.getAsDouble()), 50), Color.GREEN, 2, Color.RED);
                            circle.setZIndex(100.0f);
                        } else {
                            JsonElement array0 = coordinates.getAsJsonArray().get(0);
                            JsonElement array1 = coordinates.getAsJsonArray().get(1);

                            Log.d("CoordinatesCirEND", array0 + "------" + array1);

                            Log.d("Sep", "-----------------------------------------");

                            end_latitude =  array0.getAsDouble();
                            end_longitude = array1.getAsDouble();

                            CircleMapObject circle = mapObjects.addCircle(
                                    new Circle(new Point(array0.getAsDouble(), array1.getAsDouble()), 50), Color.RED, 2, Color.RED);
                            circle.setZIndex(100.0f);
                        }
                    } else {

                        try {
                            ArrayList<Point> polylinePoints = new ArrayList<>();
                            for (JsonElement arraySubDetail : coordinates) {
                                JsonElement array0 = arraySubDetail.getAsJsonArray().get(0);
                                JsonElement array1 = arraySubDetail.getAsJsonArray().get(1);
                                Log.d("Coordinates", array0 + "------" + array1);


                                polylinePoints.add(new Point(array0.getAsDouble(), array1.getAsDouble()));

                            }
                            origin = polylinePoints.get(0);

                            mapView.getMap().move(
                                    new CameraPosition(origin, 15.0f, 0.0f, 0.0f),
                                    new Animation(Animation.Type.SMOOTH, 5),
                                    null
                            );


                            PolylineMapObject polyline = mapObjects.addPolyline(new Polyline(polylinePoints));
                            polyline.setStrokeColor(Color.RED);
                            polyline.setZIndex(100.0f);


                        } catch (Exception e) {
                            Log.d("ExFuck", e.getMessage());

                        }

                    }


                }

                /*Toast.makeText(MapActivity.this, arrayFromString.toString(), Toast.LENGTH_LONG).show();*/

            }

            @Override
            public void onFailure(Call<AnswerLogin> call, Throwable t) {
                Toast.makeText(MapActivity.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // No explanation needed; request the permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    COARSE_LOCATION);

        } else {

            // Установка слоя для отрисовки пользовательского местоположения
            userLocationLayer = mapView.getMap().getUserLocationLayer();
            userLocationLayer.setEnabled(true);
            userLocationLayer.setAutoZoomEnabled(true);
            userLocationLayer.setHeadingEnabled(true);
            userLocationLayer.setObjectListener(this);
        }

        //here we receive the users last location
        /*fusedLocationClient.getLastLocation()
            .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {

                        //Toast.makeText(MapActivity.this, ""+ location.getLatitude()+ " "+ location.getLongitude(), Toast.LENGTH_SHORT).show();
                    }
                }
            });*/


        while (isRoadStarted){
            return;
        }
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {

                    Toast.makeText(MapActivity.this, "Updating Location: "+location.getLatitude()+" "+location.getLongitude(), Toast.LENGTH_SHORT).show();

                    //Toast.makeText(MapActivity.this, "Start Location: "+start_latitude+" "+start_longitude, Toast.LENGTH_LONG).show();

                    DecimalFormat formatter = new DecimalFormat("00.00000000000000");
                    DecimalFormat formatter2 = new DecimalFormat("00.000000000000000");

                    String loc_lat = formatter.format(location.getLatitude());
                    String loc_long = formatter2.format(location.getLongitude());

                    String start_lat = formatter.format(start_latitude);
                    String start_long = formatter2.format(start_longitude);

                    String end_lat = formatter.format(end_latitude);
                    String end_long = formatter2.format(end_longitude);

                    Log.e("TEST LAT", loc_lat+ " should be compared to "+ start_lat);
                    Log.e("TEST LONG", loc_long+ " should be compared to "+ start_long);

                    if (loc_lat.equals(start_lat) && loc_long.equals(start_long)){
                        Toast.makeText(MapActivity.this, "Your Road just started", Toast.LENGTH_SHORT).show();
                        isRoadStarted = true;
                    }

                    if (loc_lat.equals(end_lat) && loc_long.equals(end_long)){
                        Toast.makeText(MapActivity.this, "Your Road just finished", Toast.LENGTH_SHORT).show();
                        isRoadTerminated = true;
                    }
                }
            }
        };


    }



    /*protected void createLocationRequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);


        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                // All location settings are satisfied. The client can initialize
                // location requests here.
                // ...
                Toast.makeText(MapActivity.this, "YESSSSSSSSSSSSSSS", Toast.LENGTH_SHORT).show();
            }
        });


    }*/


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case COARSE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(this, "Allright", Toast.LENGTH_SHORT).show();


                } else {

                    Toast.makeText(this, "permission denied, boo!", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }


            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        startLocationUpdates();

    }


    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Sending the data only when the road has started
        //while (isRoadStarted){
        fusedLocationClient.requestLocationUpdates(locationRequest,
                locationCallback,
                null);
       // }

    }



    @Override
    protected void onStop() {
        // Вызов onStop нужно передавать инстансам MapView и MapKit.
        mapView.onStop();
        MapKitFactory.getInstance().onStop();
        super.onStop();
    }

    @Override
    protected void onStart() {
        // Вызов onStart нужно передавать инстансам MapView и MapKit.
        super.onStart();
        MapKitFactory.getInstance().onStart();
        mapView.onStart();
    }

    @Override
    public void onObjectAdded(@NonNull UserLocationView userLocationView) {

        userLocationLayer.setAnchor(
                new PointF((float) (mapView.getWidth() * 0.5), (float) (mapView.getHeight() * 0.5)),
                new PointF((float) (mapView.getWidth() * 0.5), (float) (mapView.getHeight() * 0.83)));

        // При определении направления движения устанавливается следующая иконка
        userLocationView.getArrow().setIcon(ImageProvider.fromResource(
                this, R.drawable.user_arrow));
        // При получении координат местоположения устанавливается следующая иконка
        userLocationView.getPin().setIcon(ImageProvider.fromResource(
                this, R.drawable.user_arrow));
        // Обозначается точность определения местоположения с помощью окружности
        userLocationView.getAccuracyCircle().setFillColor(Color.BLUE);
    }

    @Override
    public void onObjectRemoved(@NonNull UserLocationView userLocationView) {

    }

    @Override
    public void onObjectUpdated(@NonNull UserLocationView userLocationView, @NonNull ObjectEvent objectEvent) {

    }
}
